package com.murphy.algorithm.stack;
/**
 * 基于数组实现栈
 * 栈：先进后出，后进先出
 * @author dongsufeng
 * @date 2020/9/8 11:00
 * @version 1.0
 */
public class ArrayStack {

	private String [] items;
	//目前个数
	private int num;
	//最多支持数
	private int size;

	ArrayStack(int size){
		this.items=new String[size];
		this.num = 0;
		this.size=size;
	}

	/**
	 * 入栈
	 */
	public boolean push(String item){
		if (item == null || num == size){
			return false;
		}
		items[num] = item;
		++ num;
		return true;
	}

	/**
	 * 出栈
	 */
	public String pop(){
		if (num == 0){
			return null;
		}
		String item = items[num-1];
		-- num;
		return item;
	}

	public static void main(String[] args) {
		ArrayStack arrayStack = new ArrayStack(10);
		arrayStack.push("111");
		arrayStack.push("222");
		arrayStack.push("333");
		arrayStack.push("444");
		arrayStack.push("555");
		final int num = arrayStack.num;
		for (int i = 0 ;i<num;i++){
			System.out.println(arrayStack.pop());
		}
		System.out.println(arrayStack.num);
	}
}
