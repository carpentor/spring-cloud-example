package com.murphy.algorithm.sort;

import com.alibaba.fastjson.JSON;

/**
 * 冒泡排序
 * @author dongsufeng
 * @date 2020/9/10 14:57
 * @version 1.0
 */
public class BubbleSort {

	public void sort(int [] items){
		//外部循环
		for (int i = 0;i < items.length; i++){
			//排序终止标识
			boolean flag=false;
			//内部循环，每次循环之后相应的末尾数据就是有序的了，下次循环可忽略
			for (int j = 0; j<items.length-i-1; j++){
				if (items[j]>items[j+1]){
					int temp = items[j];
					items[j] = items[j+1];
					items[j+1] = temp;
					flag = true;
				}
			}
			//如果内部循环没有需要交互的数据那么代表数据已经是有序的了
			if (!flag){
				break;
			}
		}
	}

	public static void main(String[] args) {
		BubbleSort sort = new BubbleSort();
		int [] items={3,2,5,1,8,7,4,6};
//		int [] items={9,8,7,6,5,4,3,2,1};
//		int [] items={1,2,3,4,5,6,7,8,9};
		sort.sort(items);
		System.out.println(JSON.toJSONString(items));
	}
}
