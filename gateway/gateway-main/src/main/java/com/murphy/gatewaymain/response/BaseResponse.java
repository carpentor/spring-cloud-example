package com.murphy.gatewaymain.response;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * @author dongsufeng
 * @date 2021/11/9 14:22
 * @version 1.0
 */
@Data
public class BaseResponse<T> implements Serializable {

	private String code;
	private String msg;
	private String randomKey;

	private T data;
	private String sign;


	public BaseResponse(){}

	public BaseResponse(String code,String msg,T data){
		this.code=code;
		this.msg=msg;
		this.data=data;
	}
	public boolean success(){
		return "1000".equals(this.code);
	}
}
