package com.murphy.gatewaymain.request;

import lombok.Data;

/**
 *
 * @author dongsufeng
 * @date 2021/10/25 14:26
 * @version 3.11.8
 */
@Data
public class XdRequest extends BaseRequest {

	/**
	 * 请求时间戳毫秒
	 */
	private String time;

	/**
	 * 内容体签名串
	 */
	private String sign;

	/**
	 * 商户渠道号
	 */
	private String channelId;

	/**
	 * 版本号
	 */
	private String version;

	/**
	 * 加解密随机密钥
	 */
	private String randomKey;

	/**
	 * 请求业务数据
	 */
	private String data;

}
