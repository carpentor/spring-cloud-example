package com.murphy.gatewaydemo.response;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * @author dongsufeng
 * @date 2021/11/9 14:27
 * @version 1.0
 */
@Data
public class OrderResponse implements Serializable {

	private String orderNo;
	private String desc;
}
