package com.example.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.entity.User;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dongsufeng
 * @version 4.0
 * @date 2019/12/2 4:19 PM
 */
@RestController
@Log4j2
public class SecurityJwtController {


    @RequestMapping("/jwt")
    public User get(@RequestAttribute String username){
        log.info("=================userInfoDTO={}",username);
        User user= new User(username,username,username);
        return user;
    }
}
