package com.murphy.security.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;


@Data
@Configuration
@Component
@ConfigurationProperties(prefix = BaseProperties.PROPERTIES_PREFIX)
public class BaseProperties {

    public static final String PROPERTIES_PREFIX = "base";
    public static final String ENABLE_REDIS_CACHE = "base.enable-redis-cache";

    /**
     * 是否开启Redis缓存，true开启，false关闭
     * 为false时，采用基于内存的ehcache缓存
     */
    private boolean enableRedisCache;
}
